module.exports = {
  mode: "jit",
  darkMode: "class", // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {},
  plugins: [require("@tailwindcss/typography")],
  content: ["./_site/**/*.{md,njk,sass}"],
};
