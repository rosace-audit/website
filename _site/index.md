---
title: Cabinet de Commissariat aux Comptes
description: Cabinet de Commissariat aux Comptes, membre de la Compagnie Régionale des Commissaires aux Comptes
layout: full
tags: [fr, page]
published_at: 2023-08-08 00:00:00
redirect_from: [/fr/]
order: 1
lang: fr
---

<div class="relative isolate px-6 lg:px-8 dark:bg-gray-900">
  <div class="mx-auto max-w-2xl py-8">
    <div class="text-center">
    <h1>
      <img width="490" height="384"
        class="mx-auto h-96 w-123 dark:hidden"
        src="/images/rosace-audit-text-horizontal.svg"
        alt="{{ config.siteTitle }} logo"
        title="{{ config.siteTitle }}"
      />
      <img width="490" height="384"
          class="mx-auto h-96 w-123 hidden dark:inline"
          src="/images/rosace-audit-text-dark.svg"
          alt="{{ config.siteTitle }} logo"
          title="{{ config.siteTitle }}"
        />
        <span class="sr-only">{{ config.siteTitle }}</span>
        </h1>
      <p class="-mt-12 mb-12 text-2xl leading-8 text-gray-600 dark:text-gray-400">{{ title }}</p>
      <p class="-mt-12 mb-12 text-l leading-8 text-gray-600 dark:text-gray-400">Membre de la Compagnie Régionale des Commissaires aux Comptes</p>
      <p class="mt-6 text-lg leading-8 text-gray-600 dark:text-gray-400">Vous offrir la meilleure expérience avec notre accompagnement et notre expertise uniques</p>
      <p>
        <a href="#values" class="inline-flex items-center rounded-md bg-red-50 px-2 py-1 text-xs font-medium text-red-700 ring-1 ring-inset ring-red-600/10">Régularité</a>
        <a href="#values" class="inline-flex items-center rounded-md bg-yellow-50 px-2 py-1 text-xs font-medium text-yellow-800 ring-1 ring-inset ring-yellow-600/20">Obligation légale</a>
        <a href="#values" class="inline-flex items-center rounded-md bg-green-50 px-2 py-1 text-xs font-medium text-green-700 ring-1 ring-inset ring-green-600/20">Sincérité</a>
        <a href="#values" class="inline-flex items-center rounded-md bg-blue-50 px-2 py-1 text-xs font-medium text-blue-700 ring-1 ring-inset ring-blue-700/10">Assurance raisonnable</a>
        <a href="#values" class="inline-flex items-center rounded-md bg-indigo-50 px-2 py-1 text-xs font-medium text-indigo-700 ring-1 ring-inset ring-indigo-700/10">Conformité</a>
        <a href="#values" class="inline-flex items-center rounded-md bg-purple-50 px-2 py-1 text-xs font-medium text-purple-700 ring-1 ring-inset ring-purple-700/10">Exhaustivité</a>
      </p>
      <div class="mt-10 flex items-center justify-center gap-x-6">
        <a title="Accéder à la page entreprise" href="/fr/company/" class="text-sm font-semibold leading-6 text-gray-900 dark:text-gray-100">En savoir plus <span aria-hidden="true">→</span></a>
      </div>
    </div>
  </div>
  <div class="absolute inset-x-0 top-[calc(100%-13rem)] -z-10 transform-gpu overflow-hidden blur-3xl sm:top-[calc(100%-30rem)]" aria-hidden="true">
    <div class="relative left-[calc(50%+3rem)] aspect-[1155/678] w-[36.125rem] -translate-x-1/2 bg-gradient-to-tr from-[#60a5fa] to-[#1e40af] opacity-30 sm:left-[calc(50%+36rem)] sm:w-[72.1875rem]" style="clip-path: polygon(74.1% 44.1%, 100% 61.6%, 97.5% 26.9%, 85.5% 0.1%, 80.7% 2%, 72.5% 32.5%, 60.2% 62.4%, 52.4% 68.1%, 47.5% 58.3%, 45.2% 34.5%, 27.5% 76.7%, 0.1% 64.9%, 17.9% 100%, 27.6% 76.8%, 76.1% 97.7%, 74.1% 44.1%)"></div>
  </div>
</div>

<!-- HERO -->
<div class="relative isolate overflow-hidden bg-gray-900 py-24 sm:py-32 dark:bg-gray-100">
  <div
    class="hidden sm:absolute sm:-top-10 sm:right-1/2 sm:-z-10 sm:mr-10 sm:block sm:transform-gpu sm:blur-3xl"
    aria-hidden="true"
  >
    <div
      class="aspect-[1097/845] w-[68.5625rem] bg-gradient-to-tr from-[#60a5fa] to-[#1e40af]   opacity-20"
      style="
        clip-path: polygon(
          74.1% 44.1%,
          100% 61.6%,
          97.5% 26.9%,
          85.5% 0.1%,
          80.7% 2%,
          72.5% 32.5%,
          60.2% 62.4%,
          52.4% 68.1%,
          47.5% 58.3%,
          45.2% 34.5%,
          27.5% 76.7%,
          0.1% 64.9%,
          17.9% 100%,
          27.6% 76.8%,
          76.1% 97.7%,
          74.1% 44.1%
        );
      "
    ></div>
  </div>
  <div
    class="absolute -top-52 left-1/2 -z-10 -translate-x-1/2 transform-gpu blur-3xl sm:top-[-28rem] sm:ml-16 sm:translate-x-0 sm:transform-gpu"
    aria-hidden="true"
  >
    <div
      class="aspect-[1097/845] w-[68.5625rem] bg-gradient-to-tr from-[#60a5fa] to-[#1e40af] opacity-20"
      style="
        clip-path: polygon(
          74.1% 44.1%,
          100% 61.6%,
          97.5% 26.9%,
          85.5% 0.1%,
          80.7% 2%,
          72.5% 32.5%,
          60.2% 62.4%,
          52.4% 68.1%,
          47.5% 58.3%,
          45.2% 34.5%,
          27.5% 76.7%,
          0.1% 64.9%,
          17.9% 100%,
          27.6% 76.8%,
          76.1% 97.7%,
          74.1% 44.1%
        );
      "
    ></div>
  </div>
  <div class="mx-auto max-w-7xl px-6 lg:px-8">
    <div class="mx-auto max-w-2xl lg:mx-0">
      <h2 class="text-4xl font-bold tracking-tight text-white sm:text-6xl dark:text-gray-900">
        Travailler avec nous
      </h2>
      <p class="mt-6 text-lg leading-8 text-gray-300 dark:text-gray-700">
        Nous accompagnons les dirigeants d'entreprises et d'associations
        dans chaque étape du développement de leur entité.
      </p>
    </div>
    <div class="mx-auto mt-10 max-w-2xl lg:mx-0 lg:max-w-none">
      <div
        class="grid grid-cols-1 gap-x-8 gap-y-6 text-base font-semibold leading-7 text-white sm:grid-cols-2 md:flex lg:gap-x-10"
      >
        <a href="#">Open roles <span aria-hidden="true">&rarr;</span></a>
        <a href="#"
          >Internship program <span aria-hidden="true">&rarr;</span></a
        >
        <a href="#">Our values <span aria-hidden="true">&rarr;</span></a>
        <a href="#"
          >Meet our leadership <span aria-hidden="true">&rarr;</span></a
        >
      </div>
      <dl
        class="mt-16 grid grid-cols-1 gap-8 sm:mt-20 sm:grid-cols-2 lg:grid-cols-4"
      >
        <div class="flex flex-col-reverse">
          <dt class="text-base leading-7 text-gray-200 dark:text-gray-800">Années d'expérience</dt>
          <dd class="text-2xl font-bold leading-9 tracking-tight text-white dark:text-gray-900">
            18
          </dd>
        </div>
        <div class="flex flex-col-reverse">
          <dt class="text-base leading-7 text-gray-200 dark:text-gray-800">
            Entreprises contrôlées
          </dt>
          <dd class="text-2xl font-bold leading-9 tracking-tight text-white dark:text-gray-900">
            300+
          </dd>
        </div>
        <div class="flex flex-col-reverse">
          <dt class="text-base leading-7 text-gray-200 dark:text-gray-800">Industries différentes</dt>
          <dd class="text-2xl font-bold leading-9 tracking-tight text-white dark:text-gray-900">
            15
          </dd>
        </div>
        <div class="flex flex-col-reverse">
          <dt class="text-base leading-7 text-gray-200 dark:text-gray-800">Durée moyenne</dt>
          <dd class="text-2xl font-bold leading-9 tracking-tight text-white dark:text-gray-900">
            6 ans
          </dd>
        </div>
      </dl>
    </div>
  </div>
</div>

<!-- OUR CLIENTS -->
<div class="bg-white py-24 sm:py-32" hidden>
  <div class="mx-auto max-w-7xl px-6 lg:px-8">
    <h2 class="text-center text-lg font-semibold leading-8 text-gray-900">Nos clients</h2>
    <div class="mx-auto mt-10 grid max-w-lg grid-cols-4 items-center gap-x-8 gap-y-10 sm:max-w-xl sm:grid-cols-6 sm:gap-x-10 lg:mx-0 lg:max-w-none lg:grid-cols-5">
      <img
        class="col-span-2 max-h-12 w-full object-contain lg:col-span-1"
        src="https://tailwindui.com/img/logos/158x48/transistor-logo-gray-900.svg"
        alt="Transistor" width="158" height="48">
      <img
        class="col-span-2 max-h-12 w-full object-contain lg:col-span-1"
        src="https://tailwindui.com/img/logos/158x48/reform-logo-gray-900.svg"
        alt="Reform" width="158" height="48">
      <img
        class="col-span-2 max-h-12 w-full object-contain lg:col-span-1"
        src="https://tailwindui.com/img/logos/158x48/tuple-logo-gray-900.svg"
        alt="Tuple" width="158" height="48">
      <img
        class="col-span-2 max-h-12 w-full object-contain sm:col-start-2 lg:col-span-1"
        src="https://tailwindui.com/img/logos/158x48/savvycal-logo-gray-900.svg"
        alt="SavvyCal" width="158" height="48">
      <img
        class="col-span-2 col-start-2 max-h-12 w-full object-contain sm:col-start-auto lg:col-span-1"
        src="https://tailwindui.com/img/logos/158x48/statamic-logo-gray-900.svg"
        alt="Statamic" width="158" height="48">
    </div>
  </div>
</div>

<div class="bg-white py-24 sm:py-32">
  <div class="mx-auto max-w-7xl px-6 lg:px-8">
    <div class="mx-auto max-w-2xl lg:text-center">
      <p class="mt-2 text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">Nos valeurs</p>
      <p class="mt-6 text-lg leading-8 text-gray-600">&nbsp;</p>
      <span id="values" />
    </div>
    <div class="mx-auto mt-16 max-w-2xl sm:mt-20 lg:mt-24 lg:max-w-4xl">
      <dl class="grid max-w-xl grid-cols-1 gap-x-8 gap-y-10 lg:max-w-none lg:grid-cols-2 lg:gap-y-16">
        <div class="relative pl-16">
          <dt class="text-base font-semibold leading-7 text-gray-900">
            <div class="absolute left-0 top-0 -mt-1 flex h-10 w-10 items-center justify-center rounded-full ring-1 ring-inset ring-red-600/10 bg-red-50 text-red-700">
              R
            </div>
            <span class="inline-flex items-center rounded-md bg-red-50 px-2 py-1 text-xs font-medium text-red-700 ring-1 ring-inset ring-red-600/10">Régularité</span>
          </dt>
          <dd class="mt-2 text-base leading-7 text-gray-600">Parmi ses missions, le commissaire au comptes exprime son opinion sur la régularité, la sincérité et l'image fidèle des comptes annuels et consolidés.</dd>
        </div>
        <div class="relative pl-16">
          <dt class="text-base font-semibold leading-7 text-gray-900">
            <div class="absolute left-0 top-0 -mt-1 flex h-10 w-10 items-center justify-center rounded-full text-yellow-800 ring-1 ring-inset ring-yellow-600/20 bg-yellow-50">
              O
            </div>
            <span class="inline-flex items-center rounded-md bg-yellow-50 px-2 py-1 text-xs font-medium text-yellow-800 ring-1 ring-inset ring-yellow-600/20">Obligation légale</span>
          </dt>
          <dd class="mt-2 text-base leading-7 text-gray-600">L’audit légal est obligatoire pour les entreprises au-dessus de certains seuils définis par la loi (mission 6 exercices), mais également pour les sociétés mères ou têtes de petits groupes et les filiales significatives qu’elles contrôlent (mission 6 ou 3 exercices).</dd>
        </div>
        <div class="relative pl-16">
          <dt class="text-base font-semibold leading-7 text-gray-900">
            <div class="absolute left-0 top-0 -mt-1 flex h-10 w-10 items-center justify-center rounded-full text-green-700 ring-1 ring-inset ring-green-600/20 bg-green-50">
              S
            </div>
            <span class="inline-flex items-center rounded-md bg-green-50 px-2 py-1  text-xs font-medium text-green-700 ring-1 ring-inset ring-green-600/20">Sincérité</span>
          </dt>
          <dd class="mt-2 text-base leading-7 text-gray-600">Le commissaire au comptes vérifie la sincérité et la concordance avec les comptes annuels des informations financières fournies à l'assemblée générale.</dd>
        </div>
        <div class="relative pl-16">
          <dt class="text-base font-semibold leading-7 text-gray-900">
            <div class="absolute left-0 top-0 -mt-1 flex h-10 w-10 items-center justify-center rounded-full text-blue-700 ring-1 ring-inset ring-blue-700/10 bg-blue-50">
              A
            </div>
            <span class="inline-flex items-center rounded-md bg-blue-50 px-2 py-1 text-xs font-medium text-blue-700 ring-1 ring-inset ring-blue-700/10">Assurance raisonnable</span>
          </dt>
          <dd class="mt-2 text-base leading-7 text-gray-600">L’objectif du commissaire au comptes est d'obtenir l'assurance raisonnable qu'aucune anomalie significative ne figure dans les comptes. Il a une obligation de moyens et non de résultat.</dd>
        </div><div class="relative pl-16">
          <dt class="text-base font-semibold leading-7 text-gray-900">
            <div class="absolute left-0 top-0 -mt-1 flex h-10 w-10 items-center justify-center rounded-full bg-indigo-50 text-indigo-700 ring-1 ring-inset ring-indigo-700/10">
              C
            </div>
            <span class="inline-flex items-center rounded-md bg-indigo-50 px-2 py-1  text-xs font-medium text-indigo-700 ring-1 ring-inset ring-indigo-700/10">Conformité</span>
          </dt>
          <dd class="mt-2 text-base leading-7 text-gray-600">La conformité consiste pour les entreprises à déployer des procédures préventives leur permettant d'éviter de s'exposer à des risques liés au non-respect de la réglementation, et à établir des états financiers conformément aux principes comptables spécifiés.</dd>
        </div>
        <div class="relative pl-16">
          <dt class="text-base font-semibold leading-7 text-gray-900">
            <div class="absolute left-0 top-0 -mt-1 flex h-10 w-10 items-center justify-center rounded-full bg-purple-50 text-purple-700 ring-1 ring-inset ring-purple-700/10">
              E
            </div>
            <span class="inline-flex items-center rounded-md bg-purple-50 px-2 py-1  text-xs font-medium text-purple-700 ring-1 ring-inset ring-purple-700/10">Exhaustivité</span>
          </dt>
          <dd class="mt-2 text-base leading-7 text-gray-600">
            <p>Il s'agit d'une assertion d'audit.</p>
            <p><b>Au bilan</b>, cela signifie que tous les actifs, tous les passifs et tous les éléments de capitaux propres qui auraient dû être enregistrés l’ont bien été, et toutes les informations connexes qui auraient dû être présentées dans les états financiers l’ont bien été.</p>
            <p><b>Au compte de résultat</b>, cela signifie que toutes les opérations et tous les événements qui auraient dû être enregistrés l’ont bien été, et toutes les informations connexes qui auraient dû être présentées dans les états financiers l’ont bien été.
            </p>
          </dd>
        </div>
      </dl>
    </div>
  </div>
</div>

<!-- OUR TEAM-->
<!-- <div class="bg-white py-24 sm:py-32 dark:bg-gray-900">
  <div class="mx-auto grid max-w-7xl gap-x-8 gap-y-20 px-6 lg:px-8 xl:grid-cols-3">
    <div class="max-w-2xl">
      <h2 class="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl dark:text-gray-100">Notre équipe</h2>
      <p class="mt-6 text-lg leading-8 text-gray-600 dark:text-gray-400">Libero fames augue nisl porttitor nisi, quis. Id ac elit odio vitae elementum enim vitae ullamcorper suspendisse.</p>
    </div>
    <ul role="list" class="grid gap-x-8 gap-y-12 sm:grid-cols-2 sm:gap-y-16 xl:col-span-2">
      {%- for item in config.team -%}

      <li>
        <div class="flex items-center gap-x-6">
          <img class="h-16 w-16 rounded-full" src="https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=facearea&amp;facepad=2&amp;w=256&amp;h=256&amp;q=80" alt="">
          <div>
            <h3 class="text-base font-semibold leading-7 tracking-tight text-gray-900 dark:text-gray-100">{{ item.name }}</h3>
            <p class="text-sm font-semibold leading-6 text-[#60a5fa]">{{ item.title }}</p>
          </div>
        </div>
      </li>

      {%- endfor -%}
    </ul>
  </div>
</div> -->
