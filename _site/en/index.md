---
title: Home
description: ROSACE audit, Statutory Auditor's Office
layout: full
tags: [en, page]
published_at: 2023-08-08 00:00:00
order: 1
---

<div class="relative isolate px-6 lg:px-8 dark:bg-gray-900">
    <div class="mx-auto max-w-2xl py-8">
      <div class="text-center">
      <img width="490" height="384"
          class="mx-auto h-96 w-123 hidden dark:inline"
          src="/images/rosace-audit-text-dark.svg"
          alt="{{ config.siteTitle }} logo"
          title="{{ config.siteTitle }}"
        />
        <img width="490" height="384"
          class="mx-auto h-96 w-123 dark:hidden"
          src="/images/rosace-audit-text-horizontal.svg"
          alt="{{ config.siteTitle }} logo"
          title="{{ config.siteTitle }}"
        />
        <p class="-mt-12 mb-12 text-2xl leading-8 text-gray-600 dark:text-gray-400">Cabinet de Commissariat aux Comptes</p>
        <p class="-mt-12 mb-12 text-l leading-8 text-gray-600 dark:text-gray-400">Membre de la Compagnie Régionale des Commissaires aux Comptes</p>
        <p class="mt-6 text-lg leading-8 text-gray-600 dark:text-gray-400">Vous offrir la meilleure expérience avec notre accompagnement et notre expertise uniques</p>
        <p>
          <span class="inline-flex items-center rounded-md bg-red-50 px-2 py-1 text-xs font-medium text-red-700 ring-1 ring-inset ring-red-600/10">Régularité</span>
          <span class="inline-flex items-center rounded-md bg-yellow-50 px-2 py-1 text-xs font-medium text-yellow-800 ring-1 ring-inset ring-yellow-600/20">Obligation légale</span>
          <span class="inline-flex items-center rounded-md bg-green-50 px-2 py-1 text-xs font-medium text-green-700 ring-1 ring-inset ring-green-600/20">Sincérité</span>
          <span class="inline-flex items-center rounded-md bg-blue-50 px-2 py-1 text-xs font-medium text-blue-700 ring-1 ring-inset ring-blue-700/10">Assurance raisonnable</span>
          <span class="inline-flex items-center rounded-md bg-indigo-50 px-2 py-1 text-xs font-medium text-indigo-700 ring-1 ring-inset ring-indigo-700/10">Conformité</span>
          <span class="inline-flex items-center rounded-md bg-purple-50 px-2 py-1 text-xs font-medium text-purple-700 ring-1 ring-inset ring-purple-700/10">Exhaustivité</span>
        </p>
        <div class="mt-10 flex items-center justify-center gap-x-6">
          <a href="/fr/company/" class="text-sm font-semibold leading-6 text-gray-900 dark:text-gray-100">En savoir plus <span aria-hidden="true">→</span></a>
        </div>
      </div>
    </div>
    <div class="absolute inset-x-0 top-[calc(100%-13rem)] -z-10 transform-gpu overflow-hidden blur-3xl sm:top-[calc(100%-30rem)]" aria-hidden="true">
      <div class="relative left-[calc(50%+3rem)] aspect-[1155/678] w-[36.125rem] -translate-x-1/2 bg-gradient-to-tr from-[#60a5fa] to-[#1e40af] opacity-30 sm:left-[calc(50%+36rem)] sm:w-[72.1875rem]" style="clip-path: polygon(74.1% 44.1%, 100% 61.6%, 97.5% 26.9%, 85.5% 0.1%, 80.7% 2%, 72.5% 32.5%, 60.2% 62.4%, 52.4% 68.1%, 47.5% 58.3%, 45.2% 34.5%, 27.5% 76.7%, 0.1% 64.9%, 17.9% 100%, 27.6% 76.8%, 76.1% 97.7%, 74.1% 44.1%)"></div>
    </div>
  </div>