---
title: Services
description: Nos services, notre accompagnement
layout: full
tags: [fr, page]
published_at: 2023-08-08 00:00:00
order: 2
---

<div class="bg-white py-24 sm:py-32">
  <div class="mx-auto max-w-7xl px-6 lg:px-8">
    <div class="mx-auto max-w-2xl lg:text-center">
      <h2 class="text-3xl font-bold leading-7 text-blue-700">Audit Légal</h2>
      <!-- <p class="mt-2 text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">Audit Légal</p> -->
    </div>
    <div class="bg-white">
      <div class="mx-auto max-w-7xl px-6 lg:px-8 text-justify">
        <p class="mt-6 text-lg leading-8 text-gray-600">
          Le commissariat aux comptes, ou contrôle légal des comptes, est une profession réglementée et indépendante qui consiste en l'examen des états financiers d'une entité, visant à vérifier leur sincérité, leur régularité, leur conformité et leur aptitude à refléter l'image fidèle de l'entité. L’audit est effectué tant pour des besoins de gestion et d'analyse interne que pour les besoins des actionnaires, salariés, donateurs ou adhérents dans le cas d'entités non marchandes, établissements financiers ou pouvoirs publics. L’audit légal contribue ainsi à la qualité et à la transparence de l'information financière et comptable émise par les entités contrôlées.
        </p>
      </div>
    </div>
    <div class="mx-auto mt-16 max-w-2xl sm:mt-20 lg:mt-24 lg:max-w-4xl">
      <dl class="grid max-w-xl grid-cols-1 gap-x-8 gap-y-10 lg:max-w-none lg:grid-cols-1 lg:gap-y-16">
        <div class="relative pl-16">
          <dt class="text-base font-semibold leading-7 text-gray-900">
            <div class="absolute left-0 top-0 flex h-10 w-10 items-center justify-center rounded-lg bg-blue-700">
              <svg class="h-6 w-6 text-white" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                <path stroke-linecap="round" stroke-linejoin="round" d="M12 16.5V9.75m0 0l3 3m-3-3l-3 3M6.75 19.5a4.5 4.5 0 01-1.41-8.775 5.25 5.25 0 0110.233-2.33 3 3 0 013.758 3.848A3.752 3.752 0 0118 19.5H6.75z" />
              </svg>
            </div>
            Certification des comptes - Commissariat aux comptes
          </dt>
          <dd class="mt-2 text-base leading-7 text-gray-600">
          <p>Pour délivrer sa certification, le Commissaire aux comptes, après son analyse des risques d'anomalies significatives dans les comptes, détermine les vérifications qu'il doit réaliser tant au niveau des procédures de contrôle interne de l'entité qu'au niveau des comptes.</p>
          <p>Il certifie les comptes annuels des entreprises (bilan, compte de résultat et annexe) et exprime et justifie, dans son rapport, son opinion sur la régularité, la sincérité des comptes et l'image fidèle qu'elles donnent de la situation de l'entité.</p>
          <p>Il effectue aussi certaines vérifications spécifiques, comme celles relatives à l'égalité entre les actionnaires, et à la sincérité des informations comptables et financières adressées aux actionnaires, notamment à travers le rapport de gestion.</p>
          </dd>
        </div>
        <div class="relative pl-16">
          <dt class="text-base font-semibold leading-7 text-gray-900">
            <div class="absolute left-0 top-0 flex h-10 w-10 items-center justify-center rounded-lg bg-blue-700">
              <svg class="h-6 w-6 text-white" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                <path stroke-linecap="round" stroke-linejoin="round" d="M16.5 10.5V6.75a4.5 4.5 0 10-9 0v3.75m-.75 11.25h10.5a2.25 2.25 0 002.25-2.25v-6.75a2.25 2.25 0 00-2.25-2.25H6.75a2.25 2.25 0 00-2.25 2.25v6.75a2.25 2.25 0 002.25 2.25z" />
              </svg>
            </div>
            Commissariat aux apports
          </dt>
          <dd class="mt-2 text-base leading-7 text-gray-600">
            <p>Le commissaire aux apports exerce la mission d’évaluation des apports en nature au capital d’une société.</p>
            <p>L’apport en nature est l’apport de tout bien, droit ou valeur (à l’exclusion de numéraire) fait à une société en contrepartie d’actions ou de parts sociales.</p>
            <p>Sa mission est exercée de manière indépendante, il ne peut pas être Commissaire aux comptes pour la même société. Le commissaire aux apports est nommé à l’unanimité des associés ou par décision du Président du Tribunal de Commerce.</p>
            <p class="mt-6">La nomination d’un commissaire aux apports est</p>
            <ul class="list-disc block ml-6">
              <li>obligatoire en SA,</li>
              <li>obligatoire dès le premier apport en nature pour une SAS, et SASU,</li>
              <li>facultative en SARL et EURL, à condition que la valeur de chaque apport soit inférieure à 30.000 euros et que la valeur totale des apports en nature soit inférieure à la moitié du capital social,
              <li>facultative en SCI et SNC.</li>
              </ul>
          </dd>
        </div>
        <div class="relative pl-16">
          <dt class="text-base font-semibold leading-7 text-gray-900">
            <div class="absolute left-0 top-0 flex h-10 w-10 items-center justify-center rounded-lg bg-blue-700">
              <svg class="h-6 w-6 text-white" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                <path stroke-linecap="round" stroke-linejoin="round" d="M7.864 4.243A7.5 7.5 0 0119.5 10.5c0 2.92-.556 5.709-1.568 8.268M5.742 6.364A7.465 7.465 0 004.5 10.5a7.464 7.464 0 01-1.15 3.993m1.989 3.559A11.209 11.209 0 008.25 10.5a3.75 3.75 0 117.5 0c0 .527-.021 1.049-.064 1.565M12 10.5a14.94 14.94 0 01-3.6 9.75m6.633-4.596a18.666 18.666 0 01-2.485 5.33" />
              </svg>
            </div>
            Augmentation ou réduction de capital
          </dt>
          <!-- <dd class="mt-2 text-base leading-7 text-gray-600">Arcu egestas dolor vel iaculis in ipsum mauris. Tincidunt mattis aliquet hac quis. Id hac maecenas ac donec pharetra eget.</dd> -->
        </div>
        <div class="relative pl-16">
          <dt class="text-base font-semibold leading-7 text-gray-900">
            <div class="absolute left-0 top-0 flex h-10 w-10 items-center justify-center rounded-lg bg-blue-700">
              <svg class="h-6 w-6 text-white" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                <path stroke-linecap="round" stroke-linejoin="round" d="M7.864 4.243A7.5 7.5 0 0119.5 10.5c0 2.92-.556 5.709-1.568 8.268M5.742 6.364A7.465 7.465 0 004.5 10.5a7.464 7.464 0 01-1.15 3.993m1.989 3.559A11.209 11.209 0 008.25 10.5a3.75 3.75 0 117.5 0c0 .527-.021 1.049-.064 1.565M12 10.5a14.94 14.94 0 01-3.6 9.75m6.633-4.596a18.666 18.666 0 01-2.485 5.33" />
              </svg>
            </div>
            Appréciation des avantages particuliers
          </dt>
          <dd class="mt-2 text-base leading-7 text-gray-600">
            <p>Lors de l’octroi d’avantages particuliers (actions de préférence émises au profit d'un ou plusieurs actionnaires nommément désignés) en cours de vie sociale d’une entreprise, leurs évaluations par un commissaire aux apports est obligatoire.</p>
            <p>La désignation du commissaire aux apports est effectuée à l’unanimité des associés ou, à défaut, par le président du tribunal de commerce compétent. C’est un commissaire aux comptes n'ayant pas réalisé depuis cinq ans et ne réalisant pas de mission au sein de la société.</p>
            <p>Le commissaire désigné va ensuite établir, sous sa responsabilité, un rapport sur l’évaluation des avantages particuliers, dans lequel il stipule notamment:</p>
            <ul class="list-disc block ml-6">
              <li>la description et l’appréciation de chacun des avantages particuliers ou  des droits particuliers attachés aux actions de préférence,</li>
              <li>le mode d’évaluation retenu et sa justification.</li>
              <li>la justification que la valeur des droits particuliers correspond au moins à la valeur nominale des actions de préférence à émettre augmentée éventuellement de la prime d'émission.</li>
            </ul>
          </dd>
        </div>
        <div class="relative pl-16">
          <dt class="text-base font-semibold leading-7 text-gray-900">
            <div class="absolute left-0 top-0 flex h-10 w-10 items-center justify-center rounded-lg bg-blue-700">
              <svg class="h-6 w-6 text-white" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                <path stroke-linecap="round" stroke-linejoin="round" d="M16.023 9.348h4.992v-.001M2.985 19.644v-4.992m0 0h4.992m-4.993 0l3.181 3.183a8.25 8.25 0 0013.803-3.7M4.031 9.865a8.25 8.25 0 0113.803-3.7l3.181 3.182m0-4.991v4.99" />
              </svg>
            </div>
            Commissariat à la fusion
          </dt>
          <dd class="mt-2 text-base leading-7 text-gray-600">
          <p>Le commissaire à la fusion est un professionnel du chiffre choisi parmi la liste des commissaires aux comptes ou des experts judiciaires près la Cour d’Appel. Il exerce sa mission de manière indépendante et ne peut déjà être commissaire aux comptes des deux sociétés qui fusionnent.</p>
          <p>Le commissaire à la fusion a pour mission d’apprécier sous sa responsabilité l’opération de fusion en vérifiant les critères d’évaluation retenus par les sociétés et par conséquent si le rapport d’échange est équitable. Le commissaire dépose ensuite un rapport sur les modalités de la fusion qui est déposé au Registre du commerce et des sociétés.</p>
          </dd>
        </div>
        <div class="relative pl-16">
          <dt class="text-base font-semibold leading-7 text-gray-900">
            <div class="absolute left-0 top-0 flex h-10 w-10 items-center justify-center rounded-lg bg-blue-700">
              <svg class="h-6 w-6 text-white" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                <path stroke-linecap="round" stroke-linejoin="round" d="M7.864 4.243A7.5 7.5 0 0119.5 10.5c0 2.92-.556 5.709-1.568 8.268M5.742 6.364A7.465 7.465 0 004.5 10.5a7.464 7.464 0 01-1.15 3.993m1.989 3.559A11.209 11.209 0 008.25 10.5a3.75 3.75 0 117.5 0c0 .527-.021 1.049-.064 1.565M12 10.5a14.94 14.94 0 01-3.6 9.75m6.633-4.596a18.666 18.666 0 01-2.485 5.33" />
              </svg>
            </div>
            Commissariat à la transformation
          </dt>
          <dd class="mt-2 text-base leading-7 text-gray-600">
            <p>En cas de transformation d’une société de quelque forme que ce soit, en la forme d’une société par actions, un ou plusieurs commissaires à la transformation doivent apprécier sous leur responsabilité la valeur des biens composant l’actif social, pour attester que les capitaux propres sont au moins égal au montant du capital social, et les avantages particuliers. Il convient de s’assurer que les biens composant l’actif social ne sont pas surévalués.</p>
            <p>La nomination d’un commissaire à la transformation ne s’applique que si la société n’a pas déjà un commissaire aux comptes.</p>
          </dd>
        </div>
        <div class="relative pl-16">
          <dt class="text-base font-semibold leading-7 text-gray-900">
            <div class="absolute left-0 top-0 flex h-10 w-10 items-center justify-center rounded-lg bg-blue-700">
              <svg class="h-6 w-6 text-white" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                <path stroke-linecap="round" stroke-linejoin="round" d="M7.864 4.243A7.5 7.5 0 0119.5 10.5c0 2.92-.556 5.709-1.568 8.268M5.742 6.364A7.465 7.465 0 004.5 10.5a7.464 7.464 0 01-1.15 3.993m1.989 3.559A11.209 11.209 0 008.25 10.5a3.75 3.75 0 117.5 0c0 .527-.021 1.049-.064 1.565M12 10.5a14.94 14.94 0 01-3.6 9.75m6.633-4.596a18.666 18.666 0 01-2.485 5.33" />
              </svg>
            </div>
            Opération sur dividendes
          </dt>
          <!-- <dd class="mt-2 text-base leading-7 text-gray-600">Arcu egestas dolor vel iaculis in ipsum mauris. Tincidunt mattis aliquet hac quis. Id hac maecenas ac donec pharetra eget.</dd> -->
        </div>
      </dl>
    </div>
  </div>
</div>

<!--  -->

<div class="bg-white py-24 sm:py-32">
  <div class="mx-auto max-w-7xl px-6 lg:px-8">
    <div class="mx-auto max-w-2xl lg:text-center">
      <h2 class="text-3xl font-bold leading-7 text-blue-700">Audit Contractuel</h2>
      <!-- <p class="mt-2 text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">Audit contractuel</p> -->
    </div>
    <div class="bg-white">
      <div class="mx-auto max-w-7xl px-6 lg:px-8 text-justify">
        <p class="mt-6 text-lg leading-8 text-gray-600">
          L'audit contractuel correspond à une mission ne répondant pas à une obligation légale mais à un besoin exprimé par l'entreprise. Il peut s’agir de missions contractuelles de diagnostic/recommandations et d’attestations.
        </p>
      </div>
    </div>
    <div class="mx-auto mt-16 max-w-2xl sm:mt-20 lg:mt-24 lg:max-w-4xl">
      <dl class="grid max-w-xl grid-cols-1 gap-x-8 gap-y-10 lg:max-w-none lg:grid-cols-1 lg:gap-y-16">
        <div class="relative pl-16">
          <dt class="text-base font-semibold leading-7 text-gray-900">
            <div class="absolute left-0 top-0 flex h-10 w-10 items-center justify-center rounded-lg bg-blue-700">
              <svg class="h-6 w-6 text-white" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                <path stroke-linecap="round" stroke-linejoin="round" d="M12 16.5V9.75m0 0l3 3m-3-3l-3 3M6.75 19.5a4.5 4.5 0 01-1.41-8.775 5.25 5.25 0 0110.233-2.33 3 3 0 013.758 3.848A3.752 3.752 0 0118 19.5H6.75z" />
              </svg>
            </div>
            Contrôle des états financiers
          </dt>
          <!-- <dd class="mt-2 text-base leading-7 text-gray-600">Morbi viverra dui mi arcu sed. Tellus semper adipiscing suspendisse semper morbi. Odio urna massa nunc massa.</dd> -->
        </div>
        <div class="relative pl-16">
          <dt class="text-base font-semibold leading-7 text-gray-900">
            <div class="absolute left-0 top-0 flex h-10 w-10 items-center justify-center rounded-lg bg-blue-700">
              <svg class="h-6 w-6 text-white" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                <path stroke-linecap="round" stroke-linejoin="round" d="M16.5 10.5V6.75a4.5 4.5 0 10-9 0v3.75m-.75 11.25h10.5a2.25 2.25 0 002.25-2.25v-6.75a2.25 2.25 0 00-2.25-2.25H6.75a2.25 2.25 0 00-2.25 2.25v6.75a2.25 2.25 0 002.25 2.25z" />
              </svg>
            </div>
            Contrôle des procédures - Contrôle interne
          </dt>
          <!-- <dd class="mt-2 text-base leading-7 text-gray-600">Sit quis amet rutrum tellus ullamcorper ultricies libero dolor eget. Sem sodales gravida quam turpis enim lacus amet.</dd> -->
        </div>
        <div class="relative pl-16">
          <dt class="text-base font-semibold leading-7 text-gray-900">
            <div class="absolute left-0 top-0 flex h-10 w-10 items-center justify-center rounded-lg bg-blue-700">
              <svg class="h-6 w-6 text-white" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                <path stroke-linecap="round" stroke-linejoin="round" d="M16.023 9.348h4.992v-.001M2.985 19.644v-4.992m0 0h4.992m-4.993 0l3.181 3.183a8.25 8.25 0 0013.803-3.7M4.031 9.865a8.25 8.25 0 0113.803-3.7l3.181 3.182m0-4.991v4.99" />
              </svg>
            </div>
            Examen du <i>reporting</i> établi selon les <a href="https://en.wikipedia.org/wiki/Handelsgesetzbuch" target="_blank">normes allemandes HGB</a> et des instructions de la maison-mère
          </dt>
          <!-- <dd class="mt-2 text-base leading-7 text-gray-600">Quisque est vel vulputate cursus. Risus proin diam nunc commodo. Lobortis auctor congue commodo diam neque.</dd> -->
        </div>
        <div class="relative pl-16">
          <dt class="text-base font-semibold leading-7 text-gray-900">
            <div class="absolute left-0 top-0 flex h-10 w-10 items-center justify-center rounded-lg bg-blue-700">
              <svg class="h-6 w-6 text-white" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                <path stroke-linecap="round" stroke-linejoin="round" d="M7.864 4.243A7.5 7.5 0 0119.5 10.5c0 2.92-.556 5.709-1.568 8.268M5.742 6.364A7.465 7.465 0 004.5 10.5a7.464 7.464 0 01-1.15 3.993m1.989 3.559A11.209 11.209 0 008.25 10.5a3.75 3.75 0 117.5 0c0 .527-.021 1.049-.064 1.565M12 10.5a14.94 14.94 0 01-3.6 9.75m6.633-4.596a18.666 18.666 0 01-2.485 5.33" />
              </svg>
            </div>
            Audit et diagnostique <a href="https://www.economie.gouv.fr/entreprises/responsabilite-societale-entreprises-rse" target="_blank">RSE</a>
          </dt>
          <!-- <dd class="mt-2 text-base leading-7 text-gray-600">Arcu egestas dolor vel iaculis in ipsum mauris. Tincidunt mattis aliquet hac quis. Id hac maecenas ac donec pharetra eget.</dd> -->
        </div>
      </dl>
    </div>
  </div>
</div>

