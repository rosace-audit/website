---
title: Contact
description: Les informations pour nous contacter
layout: page
tags: [fr, page]
published_at: 2023-08-08 00:00:00
order: 4
---

<div class="mt-2 float-right text-right">
  <a class="text-lg" href="/profiles/caroline_rasigade.vcf" target="_blank">
    <button type="button" class="bg-gray-200 -m-2.5 inline-flex items-center justify-center rounded-md p-2.5 text-gray-700 dark:bg-gray-100">
      <svg class="fill-current w-4 h-4 mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M13 8V2H7v6H2l8 8 8-8h-5zM0 18h20v2H0v-2z"/></svg>
      <span>vCard</span>
    </button>
  </a>

  <div class="hidden md:block">
    <img alt="vCard QR Code" src="/profiles/caroline_rasigade_qrcode.png" width="240">
  </div>
</div>



<h1>Contact</h1>



## Email

<span class="ml-4" itemprop="email" ><a href="mailto:{{ config.contact.email }}" target="_blank">{{ config.contact.email }}</a></span>

## Téléphone

<span class="ml-4" itemprop="telephone"><a href="tel:{{ config.contact.phone }}" target="_blank">{{ config.contact.phone }}</a></span>

