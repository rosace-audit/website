---
title: Cabinet
description: Information sur notre cabinet et notre historique
layout: page
tags: [fr, page]
published_at: 2023-08-08 00:00:00
order: 3
---

# Cabinet

Diplômée du [CAFCAC](https://cdn.cncc.fr/download/cncc-flyer-cafcac.pdf) en [2019](https://cdn.cncc.fr/download/cafcac-2018-liste-des-candidats-admis.pdf), [Caroline RASIGADE](https://www.linkedin.com/in/caroline-rasigade-215a1340), commissaire aux comptes et expert comptable, décide de créer [{{ config.siteTitle }}]({{ config.url}}) après 18 années au sein de cabinets de commissariat aux comptes à Lyon et à Paris.

Elle a travaillé ces 15 dernières années dans un cabinet franco-allemand développant ainsi une expertise dans le contrôle des comptes français des filiales de groupes allemands ainsi que dans le contrôle des _reportings_ en _German Gaap_.

De part de son expérience en audit, Caroline crée une offre répondant aux besoins des dirigeants de TPE et PME.

Caroline RASIGADE est inscrite sur la liste des Commissaires aux comptes et au tableau de l’Ordre des Experts comptables.

<!-- 

<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d21466.19475239856!2d-3.2592460500000002!3d47.7373315!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sfr!4v1691786864949!5m2!1sen!2sfr" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe> -->