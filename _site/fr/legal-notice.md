---
title: Mentions légales
description: Mentions légales pour le site Internet
layout: post
tags: terms
published_at: 2023-08-08 00:00:00
---

Conformément aux dispositions de la loi n° 2004-575 du 21 juin 2004 pour la confiance en l’économie numérique, il est précisé aux utilisateurs du site {{ config.url }} l’identité des différents intervenants dans le cadre de sa réalisation et de son suivi. 

## Édition du site

Le site {{ config.url }} est édité par ROSACE audit, société par actions simplifiée au capital de 10 000 euros, dont le siège social est situé {{ config.contact.location.address }}, {{ config.contact.location.postalCode }} {{ config.contact.location.city }}, immatriculée au Registre du Commerce et des Sociétés de Nanterre sous le numéro **XXX** et dont le numéro TVA est **FRYYZZ**.

## Responsable de publication

Caroline Rasigade

## Nous contacter

- Par email : {{ config.email.contact }}
- Par téléphone : {{ config.contact.phone }}

## Hébergeur

Ce site est hébergé par GitLab, Inc., la société mère de GitLab France SAS. Le bureau de GitLab, Inc. est situé au 268 Bush Street #350 San Francisco, CA 94104-3503, États-Unis d'Amérique.
