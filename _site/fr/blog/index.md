---
title: Actualités
layout: page
tags: [fr, page]
published_at: 2023-08-08 00:00:00
order: 5
---

# Actualités

{% for item in collections.post | reverse %}

## [{{ item.data.title }}]({{ item.url }}) <small class="inline-flex items-center rounded-md bg-green-50 px-2 py-1 text-xs font-medium text-green-700 ring-1 ring-inset ring-green-600/20">{{ item.data.updated_at | date }}</small>

{{ item.data.summary | safe }}

{% endfor %}
