---js
{
  pagination: {
    data: "collections.all",
    size: 1,
    alias: "redirect",
    before: function (data) {
      return data.reduce((redirects, page) => {
        if (Array.isArray(page.data.redirect_from)) {
          for (let url of page.data.redirect_from) {
            redirects.push({ to: page.url, from: url, page: page.data });
          }
        } else if (typeof page.data.redirect_from === 'string') {
          redirects.push({ to: page.url, from: page.data.redirect_from, page: page.data });
        }
        return redirects;
      }, []);
    },
    addAllPagesToCollections: false,
  },
  permalink: "{{ redirect.from }}/index.html",
  eleventyExcludeFromCollections: true,
}
---
<!DOCTYPE html>
<html lang="fr">
  <head>
  <link rel="canonical" href="{{ redirect.to | url }}" />

  <title>Redirecting&hellip;</title>

  <!-- Icons -->
  <link rel="shortcut icon" href="/images/favicon.ico" />
  <link rel="shortcut icon" type="image/png" href="/images/x16-favicon.png" sizes="16x16" />
  <link rel="shortcut icon" type="image/png" href="/images/x32-favicon.png" sizes="32x32" />
  <link rel="shortcut icon" type="image/png" href="/images/x48-favicon.png" sizes="48x48" />
  <link rel="shortcut icon" type="image/png" href="/images/x64-favicon.png" sizes="64x64" />
  <link rel="shortcut icon" type="image/png" href="/images/x96-favicon.png" sizes="96x96" />
  <link rel="shortcut icon" type="image/png" href="/images/x128-favicon.png" sizes="128x128" />
  <link rel="shortcut icon" type="image/png" href="/images/x144-favicon.png" sizes="144x144" />
  <link rel="shortcut icon" type="image/png" href="/images/x192-favicon.png" sizes="192x192" />
  <link rel="shortcut icon" type="image/png" href="/images/x256-favicon.png" sizes="256x256" />
  <link rel="apple-touch-icon" type="image/png" href="/images/x256-favicon.png" />
  <link rel="icon" type="image/x-icon" href="/images/favicon.ico" />
  <link rel="icon" type="image/png" href="/images/x32-favicon.png" />

  <!-- Meta -->
  <meta name="description" content="{% if redirect.description %}{{ redirect.description }}{% else %}{{ config.description }}{% endif %}">
  <meta name="keywords" content="{% if keywords %}{{ keywords }}{% else %}{{ config.keywords }}{% endif %}">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta http-equiv="Content-Language" content="{{ lang }}">
  <meta name="viewport" content="width=device-width,initial-scale=1">

  <!-- {{ redirect.data | dump | safe }} -->

  <!-- Facebook -->
  <meta property="og:site_name" content="{{ config.siteTitle }} - {{ config.description }}">
  <meta property="og:title" content="{% if redirect.page.title %}{{redirect.page.title}} - {% endif %}{{ config.siteTitle }}">
  <meta property="og:type" content="website">
  <meta property="og:description" content="{% if redirect.page.description %}{{ redirect.page.description }}{% else %}{{ config.description }}{% endif %}">
  <meta property="og:url" content="{{ config.url }}{{ page.url }}">
  <meta property="og:image" content="{% if redirect.page.image %}{{ redirect.page.image }}{% else %}{{ config.image }}{% endif %}">

  <!-- Twitter -->
  <meta name="twitter:card" content="{{ config.card }}">
  <meta name="twitter:site" content="{{ config.twitter }}">
  <meta name="twitter:title" content="{% if redirect.page.title %}{{redirect.page.title}} - {% endif %}{{ config.siteTitle }}">
  <meta name="twitter:description" content="{% if redirect.page.description %}{{ redirect.page.description }}{% else %}{{ config.description }}{% endif %}">
  <meta name="twitter:creator" content="{{ config.twitter }}">
  <meta name="twitter:image" content="{{ config.url }}{{ page.url }}">
  <meta name="twitter:domain" content="{% if redirect.page.image %}{{ redirect.page.image }}{% else %}{{ config.image }}{% endif %}">

  <script>
    location = '{{ redirect.to | url }}';
  </script>

  <meta http-equiv="refresh" content="0; url={{ redirect.to | url }}" />
  <meta name="robots" content="noindex" />
  
  <h1>Redirecting&hellip;</h1>
  
  <a href="{{ redirect.to | url }}">Click here if you are not redirected.</a>
</html>